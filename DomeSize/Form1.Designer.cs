﻿namespace DomeSize
{
    partial class Form1
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージド リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.calcNumBox = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.clearBtn = new System.Windows.Forms.Button();
            this.mainusBtn = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.plusButton = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.calcBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // calcNumBox
            // 
            this.calcNumBox.Location = new System.Drawing.Point(22, 84);
            this.calcNumBox.Name = "calcNumBox";
            this.calcNumBox.Size = new System.Drawing.Size(163, 19);
            this.calcNumBox.TabIndex = 1;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(65, 159);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(36, 36);
            this.button1.TabIndex = 2;
            this.button1.Text = "2";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.NumButtonClicked);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(108, 159);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(36, 36);
            this.button2.TabIndex = 3;
            this.button2.Text = "3";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.NumButtonClicked);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(22, 201);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(36, 36);
            this.button3.TabIndex = 4;
            this.button3.Text = "4";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.NumButtonClicked);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(65, 201);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(36, 36);
            this.button4.TabIndex = 5;
            this.button4.Text = "5";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.NumButtonClicked);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(108, 201);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(36, 36);
            this.button5.TabIndex = 6;
            this.button5.Text = "6";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.NumButtonClicked);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(22, 243);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(36, 36);
            this.button6.TabIndex = 7;
            this.button6.Text = "7";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.NumButtonClicked);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(22, 159);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(36, 36);
            this.button7.TabIndex = 8;
            this.button7.Text = "1";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.NumButtonClicked);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(66, 243);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(36, 36);
            this.button8.TabIndex = 9;
            this.button8.Text = "8";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.NumButtonClicked);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(108, 243);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(36, 36);
            this.button9.TabIndex = 10;
            this.button9.Text = "9";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.NumButtonClicked);
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(22, 285);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(36, 36);
            this.button10.TabIndex = 11;
            this.button10.Text = "0";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.NumButtonClicked);
            // 
            // clearBtn
            // 
            this.clearBtn.Location = new System.Drawing.Point(150, 160);
            this.clearBtn.Name = "clearBtn";
            this.clearBtn.Size = new System.Drawing.Size(36, 36);
            this.clearBtn.TabIndex = 14;
            this.clearBtn.Text = "C";
            this.clearBtn.UseVisualStyleBackColor = true;
            this.clearBtn.Click += new System.EventHandler(this.ClearButtonClicked);
            // 
            // mainusBtn
            // 
            this.mainusBtn.Location = new System.Drawing.Point(150, 285);
            this.mainusBtn.Name = "mainusBtn";
            this.mainusBtn.Size = new System.Drawing.Size(36, 36);
            this.mainusBtn.TabIndex = 15;
            this.mainusBtn.Text = "-";
            this.mainusBtn.UseVisualStyleBackColor = true;
            this.mainusBtn.Click += new System.EventHandler(this.OperatorClicked);
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(66, 285);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(36, 36);
            this.button11.TabIndex = 16;
            this.button11.Text = ".";
            this.button11.UseVisualStyleBackColor = true;
            // 
            // plusButton
            // 
            this.plusButton.Location = new System.Drawing.Point(108, 285);
            this.plusButton.Name = "plusButton";
            this.plusButton.Size = new System.Drawing.Size(36, 36);
            this.plusButton.TabIndex = 17;
            this.plusButton.Text = "+";
            this.plusButton.UseVisualStyleBackColor = true;
            this.plusButton.Click += new System.EventHandler(this.OperatorClicked);
            // 
            // button14
            // 
            this.button14.Location = new System.Drawing.Point(151, 201);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(36, 36);
            this.button14.TabIndex = 18;
            this.button14.Text = "*";
            this.button14.UseVisualStyleBackColor = true;
            this.button14.Click += new System.EventHandler(this.OperatorClicked);
            // 
            // button15
            // 
            this.button15.Location = new System.Drawing.Point(150, 243);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(36, 36);
            this.button15.TabIndex = 19;
            this.button15.Text = "/";
            this.button15.UseVisualStyleBackColor = true;
            this.button15.Click += new System.EventHandler(this.OperatorClicked);
            // 
            // calcBtn
            // 
            this.calcBtn.Location = new System.Drawing.Point(65, 127);
            this.calcBtn.Name = "calcBtn";
            this.calcBtn.Size = new System.Drawing.Size(75, 23);
            this.calcBtn.TabIndex = 20;
            this.calcBtn.Text = "計算";
            this.calcBtn.UseVisualStyleBackColor = true;
            this.calcBtn.Click += new System.EventHandler(this.OperatorClicked);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(207, 338);
            this.Controls.Add(this.calcBtn);
            this.Controls.Add(this.button15);
            this.Controls.Add(this.button14);
            this.Controls.Add(this.plusButton);
            this.Controls.Add(this.button11);
            this.Controls.Add(this.mainusBtn);
            this.Controls.Add(this.clearBtn);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.calcNumBox);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox calcNumBox;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button clearBtn;
        private System.Windows.Forms.Button mainusBtn;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button plusButton;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Button calcBtn;
    }
}

