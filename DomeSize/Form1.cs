﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DomeSize
{
    public partial class Form1 : Form
    {
        string InputNumString;

        public Form1()
        {
            InitializeComponent();
        }


        private void NumButtonClicked(object sender, EventArgs e)
        {
            // senderの詳しい情報を取り扱えるようにする
            Button btn = (Button)sender;
            //押されたボタンの数字
            string btnText = btn.Text;

            //入力された数字に連結する
            InputNumString += btnText;

            //画面上に数字を出す
            calcNumBox.Text = InputNumString;



        }

        private void ClearButtonClicked(object sender, EventArgs e)
        {

            InputNumString = "";
            calcNumBox.Text = InputNumString;

        }

        
            private void OperatorClicked(object sender, EventArgs e)
            {
                int value;//TryParse用変数
                int inputNum;//数値変換した値
                int result = 0;//計算結果
                string Operator = null;



                int num1 = result;
                int num2 = int.Parse(InputNumString);

                bool isNumber = int.TryParse(calcNumBox.Text, out value);
                if (isNumber)
                {
                    inputNum = value;
                }
                else if (Operator == "+")
                {
                    result = num1 + num2;
                    calcNumBox.Text = result.ToString();

                }
                else if(Operator == "-")
            {
                result = num1 - num2;
                calcNumBox.Text = result.ToString();
            }else if(Operator == "*")
            {
                result = num1 * num2;
                calcNumBox.Text = result.ToString();
            }else if(Operator == "/")
            {
                result = num1 / num2;
                calcNumBox.Text = result.ToString();
            }

                else
                {
                    inputNum = 0;
                }


            }

        }
    
  
       
        
        }

