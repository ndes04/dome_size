﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomeSize
{
    class Ruler
    {
        private int id;
        private string name;
        private string sizeNum;

        public Ruler(int id, string name, string sizeNum)
        {
            this.id = id;
            this.name = name;
            this.sizeNum = sizeNum;


        }

        public int Id { get => id; set => id = value; }
        public string Name { get => name; set => name = value; }
        public string SizeNum { get => sizeNum; set => sizeNum = value; }
    }
}
